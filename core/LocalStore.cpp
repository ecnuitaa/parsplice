//
//  LocalStore.cpp
//  ParSplice-refact
//
//  Created by Danny Perez on 1/9/17.
//  Copyright © 2017 dp. All rights reserved.
//

#include "LocalStore.hpp"

#include <boost/filesystem.hpp>
#include <boost/format.hpp>




/*
 * Definition of AbstractLocalDataStore
 */
AbstractLocalDataStore::AbstractLocalDataStore(){
	currentSize=0;
	maxSize=0;
};


unsigned int AbstractLocalDataStore::count(unsigned int dbKey, uint64_t &key){
	return storedKeys[dbKey].count(key);
};

void AbstractLocalDataStore::touch(unsigned int dbKey, uint64_t &key){
	if(count(dbKey,key)>0) {
		mru.touch(std::make_pair(dbKey,key));
	}
};

std::set<uint64_t> AbstractLocalDataStore::availableKeys(unsigned int dbKey){
	if(storedKeys.count(dbKey)==0) {
		return std::set<uint64_t>();
	}
	return storedKeys[dbKey];
};

std::multimap< std::pair<unsigned int, uint64_t>, Rd> AbstractLocalDataStore::purge(){
	std::multimap< std::pair<unsigned int, uint64_t>, Rd> deletedItems;

	while(maxSize>0 and currentSize>maxSize) {
		auto p=mru.oldest();
		Rd data;
		get(p.second.first,p.second.second,data);
		deletedItems.insert(std::make_pair(p.second,data));
		//currentSize-=data.size();
		erase(p.second.first,p.second.second);
		mru.pop_oldest();
	}
	return deletedItems;
};

void AbstractLocalDataStore::setMaximumSize(unsigned long maxSize_){
	maxSize=maxSize_;
};


#if 0
/*
 * Definition of LDBLocalDataStore
 */

LDBLocalDataStore::LDBLocalDataStore(){


};

LDBLocalDataStore::~LDBLocalDataStore(){
	//close the databases
	for(auto it=dbm.begin(); it!=dbm.end(); it++) {
		delete it->second;
	}
};

int LDBLocalDataStore::initialize(std::string homeDir_, std::string baseName_){


	homeDir=homeDir_;
	baseName=baseName_;


	boost::filesystem::path p(homeDir);
	std::cout<<homeDir<<" "<<baseName<<" "<<p.parent_path().string()<<std::endl;
	boost::filesystem::create_directories( p.parent_path().string() );

	return 0;
};



std::string LDBLocalDataStore::u2sKeyConv(uint64_t &key){
	char *c=reinterpret_cast<char *>(&key);
	std::string keys(c,8);
	return keys;
};

uint64_t LDBLocalDataStore::s2uKeyConv(std::string &key){
	uint64_t *keyu=reinterpret_cast<uint64_t *>(&(key[0]));
	return *keyu;
};

int LDBLocalDataStore::put(unsigned int dbKey, uint64_t &key, Rd &data){


	if( dbm.count(dbKey)==0 ) {
		return DBKEY_NOTFOUND;
	}

	if(dbm.count(dbKey)>0 and storedKeys[dbKey].count(key)==0) {
		std::string ds(data.begin(),data.end());
		leveldb::Status s = dbm[dbKey]->Put(leveldb::WriteOptions(), u2sKeyConv(key), ds);
		storedKeys[dbKey].insert(key);
		if(!s.ok()) {
			std::cerr<<"int LDBLocalDataStore::put(unsigned int dbKey, uint64_t &key, Rd &data)"<<std::endl;
			std::cerr<<"LEVELDB ERROR: "<<key<<" "<<s.ToString()<<std::endl;
		}
		assert(s.ok());
	}
	mru.touch(std::make_pair(dbKey,key));

	return 0;
};

int LDBLocalDataStore::get(unsigned int dbKey, uint64_t &key, Rd &data){

	data.clear();
	if( dbm.count(dbKey)==0 ) {
		return DBKEY_NOTFOUND;
	}
	if(storedKeys[dbKey].count(key)==0) {
		return KEY_NOTFOUND;
	}

	std::string value;
	leveldb::Status s = dbm[dbKey]->Get(leveldb::ReadOptions(), u2sKeyConv(key), &value);

	if(!s.ok()) {
		std::cerr<<"int LDBLocalDataStore::get(unsigned int dbKey, uint64_t &key, Rd &data)"<<std::endl;
		std::cerr<<"LEVELDB ERROR: "<<key<<" "<<s.ToString()<<std::endl;
	}
	assert(s.ok());

	//TODO: Optimize this
	data=std::vector<char>(value.begin(), value.end());

	bool eraseOnGet=dbAttributes[dbKey].second;
	if(eraseOnGet) {
		erase(dbKey,key);
	}
	return 0;
};


int LDBLocalDataStore::createDatabase(unsigned int dbKey, bool allowDuplicates_, bool eraseOnGet_){

	if(allowDuplicates_) {
		std::cerr<<"LDBLocalDataStore does not support allowDuplicates"<<std::endl;
	}
	assert(not allowDuplicates_);

	leveldb::Options options;
	options.create_if_missing = true;
	if(dbm.count(dbKey)==0) {
		dbAttributes[dbKey]=std::make_pair(allowDuplicates_, eraseOnGet_);
		std::string name=homeDir+str(boost::format("/%s.%i") % baseName % dbKey);
		leveldb::Status s = leveldb::DB::Open(options, name, &dbm[dbKey]);
		if(!s.ok()) {
			std::cerr<<"int LDBLocalDataStore::createDatabase(unsigned int dbKey)"<<std::endl;
			std::cerr<<"LEVELDB ERROR: "<<s.ToString()<<std::endl;
		}
		assert(s.ok());
	}

	leveldb::Iterator* it = dbm[dbKey]->NewIterator(leveldb::ReadOptions());

	//refill the key list
	for (it->SeekToFirst(); it->Valid(); it->Next()) {
		std::string k=it->key().ToString();
		uint64_t key= s2uKeyConv(k);
		storedKeys[dbKey].insert(key);
		currentSize+=it->value().size();
	}
	delete it;

	return 0;
};

int LDBLocalDataStore::sync(){

	//sync by inserting an element in synchronous mode
	if(dbm.size()>0) {
		uint64_t key=0;
		std::string ds="0";
		leveldb::WriteOptions write_options;
		write_options.sync = true;
		for(auto it=dbm.begin(); it!=dbm.end(); it++) {
			leveldb::Status s = it->second->Put(write_options, u2sKeyConv(key), ds);
		}

	}
	return 0;
};



int LDBLocalDataStore::erase(unsigned int dbKey, uint64_t &key){
	if( dbm.count(dbKey)==0 ) {
		return 0;
	}
	if(storedKeys[dbKey].count(key)==0) {
		return 0;
	}

	std::string value;
	leveldb::Status s = dbm[dbKey]->Get(leveldb::ReadOptions(), u2sKeyConv(key), &value);
	currentSize-=value.size();

	if(!s.ok()) {
		std::cout<<"int LDBLocalDataStore::erase(unsigned int dbKey, uint64_t &key)"<<std::endl;
		std::cout<<"LEVELDB ERROR: "<<s.ToString()<<std::endl;
	}
	assert(s.ok());

	s=dbm[dbKey]->Delete(leveldb::WriteOptions(), u2sKeyConv(key));
	if(!s.ok()) {
		std::cout<<"int LDBLocalDataStore::erase(unsigned int dbKey, uint64_t &key)"<<std::endl;
		std::cout<<"LEVELDB ERROR: "<<s.ToString()<<std::endl;
	}
	assert(s.ok());

	storedKeys[dbKey].erase(key);

	return value.size();
};



#endif




BDBLocalDataStore::BDBLocalDataStore(){


};

BDBLocalDataStore::~BDBLocalDataStore(){
	//close the databases
	//close the databases
	for(auto it=dbm.begin(); it!=dbm.end(); it++) {
		it->second->close(0);
		delete it->second;
	}

	delete env;
};

int BDBLocalDataStore::initialize(std::string homeDir_, std::string baseName_){


	homeDir=homeDir_;
	baseName=baseName_;
	int scratchSize=1;

	boost::filesystem::path p(homeDir);
	//std::cout<<homeDir<<" "<<baseName<<" "<<p.parent_path().string()<<std::endl;
	boost::filesystem::create_directories( p.parent_path().string() );

	//initialize the environment
	u_int32_t envFlags=
		DB_CREATE        |// Create the environment if it does not exist
		DB_PRIVATE |
		DB_RECOVER     |// Run normal recovery.
		//DB_INIT_LOCK   |  // Initialize the locking subsystem
		DB_INIT_LOG    |// Initialize the logging subsystem
		DB_INIT_TXN    |// Initialize the transactional subsystem. This
		DB_AUTO_COMMIT |
		DB_INIT_MPOOL; //   |// Initialize the memory pool (in-memory cache)
	//DB_THREAD; // Cause the environment to be free-threaded


	try {
		// create and open the environment
		u_int32_t flag=DB_CXX_NO_EXCEPTIONS;
		env = new DbEnv(flag);
		env->set_lk_detect(DB_LOCK_MINWRITE);

		env->set_error_stream(&std::cerr);
		env->set_cachesize(scratchSize, 0, 0);
		env->open(homeDir.c_str(), envFlags, 0644);
	}
	catch(DbException &e) {
		std::cerr << "Error opening database environment: "
		          << homeDir << std::endl;
		std::cerr << e.what() << std::endl;
		return (EXIT_FAILURE);
	}
	return EXIT_SUCCESS;

};



Rd BDBLocalDataStore::u2RdKeyConv(uint64_t &key){
	Rd k(sizeof(Label)/sizeof(Rdt),0);
	uint64_t *p=reinterpret_cast<uint64_t*>(&(k[0]));
	*p=key;
	return k;
};

uint64_t BDBLocalDataStore::rd2uKeyConv(Rd &key){
	Label *keyu=reinterpret_cast<Label *>(&(key[0]));
	return *keyu;
};

int BDBLocalDataStore::put(unsigned int dbKey, uint64_t &key, Rd &data){

	//std::cout<<"BDBLocalDataStore::put "<<key<<std::endl;
	if( dbm.count(dbKey)==0 ) {
		return DBKEY_NOTFOUND;
	}

	if(dbm.count(dbKey)>0 and storedKeys[dbKey].count(key)==0) {
		Rd ky=u2RdKeyConv(key);
		//convert raw data into Dbt's
		Dbt k(&(ky[0]), u_int32_t(ky.size()*sizeof(Rdt)));
		Dbt d(&(data[0]), u_int32_t(data.size()*sizeof(Rdt)));


		u_int32_t flags = DB_NOOVERWRITE;

		//put
		u_int32_t status=dbm[dbKey]->put(NULL, &k, &d, flags);

		storedKeys[dbKey].insert(key);
	}
	mru.touch(std::make_pair(dbKey,key));
	/*
	   {
	        dbstl::db_multimap<Label, dbstl::DbstlDbt > states;
	        dbstl::db_multimap<Label, dbstl::DbstlDbt >::iterator it;
	        states.set_db_handle(dbm[dbKey],env);

	        std::cout<<states.size()<<" STATES FOUND IN DB"<<std::endl;
	   }
	 */
	//std::cout<<"BDBLocalDataStore::put done"<<std::endl;
	return 0;
};

int BDBLocalDataStore::get(unsigned int dbKey, uint64_t &key, Rd &data){
	//std::cout<<"BDBLocalDataStore::get "<<key<<std::endl;
	data.clear();
	if( dbm.count(dbKey)==0 ) {
		return DBKEY_NOTFOUND;
	}
	if(storedKeys[dbKey].count(key)==0) {
		return KEY_NOTFOUND;
	}


	u_int32_t status=0;

	Rd ky=u2RdKeyConv(key);
	//convert raw data into Dbt's
	Dbt k(&(ky[0]), u_int32_t(ky.size()*sizeof(Rdt)));
	Dbt d;
	Dbc *cursorp;
	dbm[dbKey]->cursor(NULL, &cursorp, 0);
	status=cursorp->get(&k,&d,DB_SET);

	//std::cout<<status<<" "<<u_int32_t(DB_NOTFOUND)<<std::endl;
	if(status!=DB_NOTFOUND and status !=DB_KEYEMPTY) {
		//convert Dbt into raw data
		//key.clear();
		//key.resize( k.get_size()/sizeof(Rdt),0 );
		//memcpy(&(key[0]),k.get_data(), k.get_size());

		data.clear();
		data.resize( d.get_size()/sizeof(Rdt),0 );
		memcpy(&(data[0]),d.get_data(), d.get_size());


		//delete the element we just read if it was not a named constant
		bool eraseOnGet=dbAttributes[dbKey].second;
		if(eraseOnGet) {
			cursorp->del(0);
		}


	}
	cursorp->close();

/*
        bool eraseOnGet=dbAttributes[dbKey].second;
        if(eraseOnGet) {
                erase(dbKey,key);
        }
 */

	//std::cout<<"BDBLocalDataStore::get done"<<std::endl;
	return 0;
};


int BDBLocalDataStore::createDatabase(unsigned int dbKey, bool allowDuplicates_, bool eraseOnGet_){

	if(allowDuplicates_) {
		std::cerr<<"BDBLocalDataStore does not support allowDuplicates (yet)"<<std::endl;
	}
	assert(not allowDuplicates_);


	if( dbm.count(dbKey)==0 ) {
		dbm[dbKey]=new Db(env,DB_CXX_NO_EXCEPTIONS);
		dbAttributes[dbKey]=std::make_pair(allowDuplicates_, eraseOnGet_);

		//create a database name
		std::string name=boost::str(boost::format("%1%-%2%.db" ) % baseName % dbKey );

		u_int32_t openFlags = DB_CREATE;  // Allow database creation

		dbm[dbKey]->set_flags(DB_DUP); //Allow duplicates

		//open the database
		int ret=dbm[dbKey]->open(NULL, name.c_str(), NULL, DB_HASH, openFlags, 0);
		//BOOST_LOG_SEV(lg,debug) <<"#BDBLocalDataStore::createDatabase open "<<ret;

		//TODO: refill the key list

		Dbc *cursorp;
		dbm[dbKey]->cursor(NULL, &cursorp, 0);

		Dbt d;
		Dbt k;

		/*
		   std::cout<<"AVAILABLE KEYS: "<<std::endl;
		   dbstl::db_multimap<Label, dbstl::DbstlDbt > states;
		   dbstl::db_multimap<Label, dbstl::DbstlDbt >::iterator it;
		   states.set_db_handle(dbm[dbKey],env);

		   std::cout<<states.size()<<" STATES FOUND IN DB"<<std::endl;
		   int ic=0;
		   for(it=states.begin(); it!=states.end(); it++) {
		        std::cout<<ic<<" "<<it->first<<std::endl;
		        ic++;
		   }
		   std::cout<<"=========="<<std::endl;
		 */
		int ic=0;
		while(cursorp->get(&k, &d, DB_NEXT)==0 ) {
			Label *keyu=reinterpret_cast<Label *>(k.get_data());
			storedKeys[dbKey].insert(*keyu);
			//std::cout<<ic<<" "<<*keyu<<std::endl;
			ic++;
		}

		cursorp->close();
	}
	//std::cout<<"BDBLocalDataStore::createDatabase done"<<std::endl;
	return 0;
};

int BDBLocalDataStore::sync(){
	for(auto it=dbm.begin(); it!=dbm.end(); it++) {
		int ret=it->second->sync(0);
		//std::cout<<"SYNCH RET: "<<ret<<std::endl;
	}
	return 0;
};



int BDBLocalDataStore::erase(unsigned int dbKey, uint64_t &key){
	//this backend intentionally does not support erasing
	return 0;

};







/*
 * Definition of STLLocalDataStore
 */


STLLocalDataStore::STLLocalDataStore(){
	currentSize=0;
};

int STLLocalDataStore::initialize(std::string homeDir_, std::string baseName_){
	return 0;
};

int STLLocalDataStore::put(unsigned int dbKey, uint64_t &key, Rd &data){

	if( dbm.count(dbKey)==0 ) {
		return DBKEY_NOTFOUND;
	}
	bool allowDuplicates=dbAttributes[dbKey].first;

	if(allowDuplicates or count(dbKey, key)==0) {
		dbm[dbKey].insert(std::make_pair(key,data));
		storedKeys[dbKey].insert(key);
		currentSize+=data.size();
	}

	mru.touch(std::make_pair(dbKey,key));

	return 0;

};

int STLLocalDataStore::get(unsigned int dbKey, uint64_t &key, Rd &data){

	if( dbm.count(dbKey)==0 ) {
		return DBKEY_NOTFOUND;
	}

	if(dbm[dbKey].count(key)==0) {
		return KEY_NOTFOUND;
	}

	auto it=dbm[dbKey].find(key);
	data=it->second;

	bool eraseOnGet=dbAttributes[dbKey].second;
	if(eraseOnGet) {
		int size;
		size=data.size();
		currentSize-=size;
		dbm[dbKey].erase(it);
	}
	if( dbm[dbKey].count(key)==0) {
		mru.erase(std::make_pair(dbKey,key));
		storedKeys[dbKey].erase(key);
	}
	else{
		mru.touch(std::make_pair(dbKey,key));
	}

	return 0;
};


int STLLocalDataStore::createDatabase(unsigned int dbKey, bool allowDuplicates_, bool eraseOnGet_){
	if(dbm.count(dbKey)==0) {
		dbm[dbKey]=std::unordered_multimap< uint64_t, Rd>();
		dbAttributes[dbKey]=std::make_pair(allowDuplicates_, eraseOnGet_);
	}
	return 0;
};

int STLLocalDataStore::sync(){
	//This does nothing for now, but we could serialize if needed
	return 0;
};


int STLLocalDataStore::erase(unsigned int dbKey, uint64_t &key){
	int size=0;
	if(dbm[dbKey].count(key)!=0) {
		auto it=dbm[dbKey].find(key);
		size=it->second.size();
		dbm[dbKey].erase(it);

		if(dbm[dbKey].count(key)==0) {
			mru.erase(std::make_pair(dbKey,key));
			storedKeys[dbKey].erase(key);
		}
	}
	currentSize-=size;

	return size;
};
