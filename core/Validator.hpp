#ifndef Validator_hpp
#define Validator_hpp

#include <stdio.h>

#include <set>
#include <vector>
#include <list>
#include <limits>
#include <deque>
#include <chrono>

#include "Types.hpp"


struct Placeholder {
	Placeholder(){
		queued=std::chrono::high_resolution_clock::now();
	}
	int producerID;
	SegmentDatabase segments;
	std::chrono::high_resolution_clock::time_point queued;
};



class Validator {
public:
void init(std::set<int> clients_,int batchSize_,bool sl_=false);
void validate(int peer, SegmentDatabase &batch);
void refreshHead(int timeout);
SegmentDatabase release();

private:
bool sl;
int ib;
std::set<int> clients;
int batchSize;
std::list< Placeholder > unvalidated;
//std::deque< Placeholder > unvalidated;
std::unordered_map< int, std::list< Placeholder >::iterator > locations;
//std::unordered_map< int, int > placeholderIndex;
SegmentDatabase ready;

};

/*
   class ValidatorV {
   public:

   void init(std::set<int> clients_,int batchSize_);
   void validate(int peer, SegmentDatabase &batch);
   SegmentDatabase release();

   private:
   int ib;
   std::set<int> clients;
   int batchSize;
   std::deque< Placeholder > unvalidated;
   std::map< int, std::set<int> > indices;
   SegmentDatabase ready;

   };
 */

#endif
