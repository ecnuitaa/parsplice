
import sys
import string
import networkx as nx


file=sys.argv[1]

instr=open(file,'r')

sl=[]
for l in instr.readlines():
    s=string.split(l)
    try:
        sl.append(int(s[0]))
    except:
        pass

instr.close()

G=nx.DiGraph()

for i in range(len(sl)-1):
    if not (sl[i]==0 or sl[i+1]==0):
        G.add_edge(sl[i],sl[i+1])


print "#Number of nodes: ",G.number_of_nodes()
print "#Number of edges:", G.number_of_edges()

p=nx.shortest_path(G,source=sl[0],target=sl[-1])

print "#Length of the shortest path: ", len(p)
for s in p:
    print s
